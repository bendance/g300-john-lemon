﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using UnityEngine;

using TMPro;

public class Swords : MonoBehaviour
{
    public GameObject sword;
    public GameObject ghost;
    bool swordswung;

    public GameObject player;
    public GameObject exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;

    int GhostsAlive = 6;

    public TextMeshProUGUI countText;

    // Start is called before the first frame update
    void Start()
    {
        swordswung = false;
        exitBackgroundImageCanvasGroup.SetActive(false);
        print(GhostsAlive);


        SetCountText();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            UnityEngine.Debug.Log("You swung me and the swords script works");
            swordswung = true;
            Invoke("SwordTrigger", 1.0f);
        }

        if (GhostsAlive <= 0)
        {
            player.SetActive(false);
            exitBackgroundImageCanvasGroup.SetActive(true);
            exitAudio.Play(0);
            Invoke("StartReset", 2.0f);
        }
    }

    private void SwordTrigger()
    {
        swordswung = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("enemy") && swordswung == true)
        {
            other.gameObject.SetActive(false);

            GhostsAlive = GhostsAlive - 1;
            print(GhostsAlive);

            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Ghosts: " + GhostsAlive.ToString();
    }

    void StartReset()
    {
        SceneManager.LoadScene("monsterstart");
    }
}