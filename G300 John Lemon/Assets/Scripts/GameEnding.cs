﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public GameObject caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public TextMeshProUGUI healthText;

    private int health;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    void Start()
    {
        caughtBackgroundImageCanvasGroup.SetActive(false);
        health = 3;
        SetHealthText();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("enemy"))
        {
            health = health - 1;

            SetHealthText();
        }
    }


    void SetHealthText()
    {
        healthText.text = "Health: " + health.ToString();

        if (health <= 0)
        {
            player.SetActive(false);
            caughtBackgroundImageCanvasGroup.SetActive(true);
            caughtAudio.Play(0);
            Invoke("Reset", 2.0f);
        }
    }

    void Update()
    {
        
    }

    void Reset()
    {
        SceneManager.LoadScene("monsterslayer");
    }
}